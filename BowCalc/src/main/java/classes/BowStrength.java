/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Bartosz
 */
public class BowStrength {

    public static String choosingDrawWeightForW(int x) {
        String strenght;
        switch (x) {
            case 0:
                strenght = "mniejszy niż 10kg";
                break;
            case 1:
                strenght = "10kg-14kg";
                break;
            case 2:
                strenght = "14kg-18kg";
                break;
            default:
                strenght = "Wybież tęzyznę!";
                break;
        }
        return strenght;
    }
    public static String choosingDrawWeightForM(int x) {
        String strenght;
        switch (x) {
            case 0:
                strenght = "mniejszy niż 12kg";
                break;
            case 1:
                strenght = "12kg-16kg";
                break;
            case 2:
                strenght = "16kg-20kg";
                break;
            default:
                strenght = "Wybież tęzyznę!";
                break;
                
        }
        
        return strenght;
    }
    public static String choosingDrawWeightForWH(int x) {
        return "Więcej niż 18kg";
    }
    public static String choosingDrawWeightForMH(int x) {
        return "Więcej niż 20kg";
    }
}
