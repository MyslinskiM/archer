<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 12.12.2018
 * Time: 22:33
 */
include("scripts/server.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Rejestracja</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">
    <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>
</header>
<nav>


</nav>
<main>
    <div class="container"  >
        <form action="Register.php" method="post" class="form-group">
            <?php
            include("scripts/errors.php");
            ?>
            <label for = "username">Login:</label><br>
            <input type="text" name="username" placeholder="Podaj Login" class="form-control" required><br>
            <label for = "email">Email:</label><br>
            <input type="email" name="email" placeholder="Podaj Email" class="form-control" required><br>
            <label for = "password">Hasło:</label><br>
            <input type="password" name="password1" placeholder="Podaj Hasło" class="form-control" required><br>
            <label for="password2">Powtórz hasło:</label><br>
            <input type="password" name="password2" placeholder="Powtórz hasło" class="form-control" required><br>
            <br>
            <input type="submit" value="Rejestruj!" name="register" class="btn btn-info">
        </form>
    </div>
</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>
