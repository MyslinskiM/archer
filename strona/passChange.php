<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 20.12.2018
 * Time: 18:33
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 07.12.2018
 * Time: 01:58
 */
include("scripts/session.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Archer</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">
    <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>

</header>
<nav>


</nav>
<main>
    <form action="confirm.php" method="post">
        <input type="hidden" name="user" value="<?php echo $_SESSION['username'] ?>">
        <label for="password">wprowadź nowe hasło</label><br>
        <input name="password" type="password" placeholder="******"><br>
        <label for="password1">potwierdź nowe hasło</label><br>
        <input name="password1" type="password" placeholder="******"><br>
        <br>
        <input type="submit" name="changePassword" value="zapisz" class="btn btn-info">
    </form>
</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>
