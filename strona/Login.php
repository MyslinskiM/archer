<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 12.12.2018
 * Time: 22:33
 */
include("scripts/server.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Logowanie</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">

    <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>
</header>
<nav>

</nav>
<main>
    <div class="container"  >
        <form action="Login.php" method="post" class="form-group">
            <label for = "username">Login/email</label><br>
            <input type="text" name="username" placeholder="Podaj Login lub email" class="form-control" required><br>
            <label for = "password">Hasło:</label><br>
            <input type="password" name="password" placeholder="Podaj Hasło" class="form-control" required><br>
            <br>
            <input type="submit" value="Loguj!" name ="login" class="btn btn-info"><br>
        </form>
    </div>
</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>
