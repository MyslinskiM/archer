<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 07.12.2018
 * Time: 01:58
 */
include("scripts/session.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Dodawanie profilu łucznika</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">
    <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>

</header>
<nav>


</nav>
<main>
    <form action="confirmInsert.php" method="post" class="form-group">
        <input type="hidden" name = "login" value="<?php echo $_SESSION['username']; ?>">
        <input type ="radio" name="gender" value="k" class="radio" checked>Kobieta<br>
        <input type ="radio" name="gender" value="m" class="radio">Mężczyzna<br>
        <label for = "height">Wzrost (w centymetrach)</label>
        <input type="text" name ="height" class="form-control"  placeholder="Wzrost (w centymetrach)"><br>
        Tężyzna:<br>
        <input type="radio" name ="power" value="1" class="radio" checked>Niska<br>
        <input type="radio" name ="power" value="2" class="radio">Średnia<br>
        <input type="radio" name ="power" value="3" class="radio">Wysoka<br>
        <input type="radio" name ="power" value="4" class="radio">Pro<br>
        <input type="submit" value="zapisz" class="btn btn-info">
    </form>
</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>