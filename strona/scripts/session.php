<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 13.12.2018
 * Time: 00:48
 */
session_start();

if (!isset($_SESSION['username'])&&$_SERVER["PHP_SELF"]=='index.php') {
    $_SESSION['msg'] = "Zaloguj się !";
    header('location: Login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: index.php");
}
?>