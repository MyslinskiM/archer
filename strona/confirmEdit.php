<!DOCTYPE html>
<html>
<head>
    <title>Archer</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">
    <img src="images/logo.png" class="img-responsive">
    <span style="font-size:45px;cursor:pointer" onclick="openNav()" id ="button">&#9776;</span>
</header>
<nav>
    <div id ="mySidenav" class = "sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href = "index.php"><img src="images/home.png" class="icon"></a>
        <a href = "profil.php">Twój profil</a>
        <a href="chooseWeapon.php">Dobór łuku</a>
        <a href="chooseArrow.php">Dobór strzał</a>
        <a href="facebook.com"><img src = "images/fb.png" class="icon"></a>
    </div>

</nav>
<main>
    <?php
    /**
     * Created by PhpStorm.
     * User: pawel
     * Date: 07.12.2018
     * Time: 02:10
     */
    include("scripts/connection.php");
    $id = $_POST['id'];
    $gender = $_POST['gender'];
    $height = $_POST['height'];
    $power = $_POST['power'];
    $sql = "update kalkulator set `gender` = '$gender',`height`='$height',`power`='$power' where id_k =$id" ;
    echo $sql;
    if(mysqli_query($conn,$sql)){
        echo '<div class="alert alert-success">rekord został zaktualizowany</div>';
    }else{
        echo '<div class="alert alert-danger">bład aktualizacji rekordu: ' . mysqli_error($conn)."</div>";
    }

    $conn->close();
    ?>
    <a href ="index.php" class="btn btn-dark">Powrót</a>
</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>
