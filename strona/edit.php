<?php
     include('scripts/session.php');
    include('scripts/connection.php');
    $id = $_GET['id'];
    $searching = "select * from kalkulator where `id_k`=".$id." ";
$res = $conn -> query($searching);
if($res ->num_rows>0) {
    while ($row = $res->fetch_assoc()) {
        $gender = $row['gender'];
        $height = $row['height'];
        $power = $row['power'];
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Archer</title>
    <link rel="Shortcut icon" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script type="text/javascript" src="scripts/slider.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header class="container">
    <img src="images/logo.png" class="img-responsive">
    <span style="font-size:45px;cursor:pointer" onclick="openNav()" id ="button">&#9776;</span>
</header>
<nav>
    <div id ="mySidenav" class = "sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href = "index.php"><img src="images/home.png" class="icon"></a>
        <a href = "profil.php">Twój profil</a>
        <a href="chooseWeapon.php">Dobór łuku</a>
        <a href="chooseArrow.php">Dobór strzał</a>
        <a href="facebook.com"><img src = "images/fb.png" class="icon"></a>
    </div>

</nav>
<main>
    <form action="confirmEdit.php" method="post">
         <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
        <label for="gender">Płeć:</label><br>
        <input type ="radio" name="gender" value="k"  <?php  if ($gender =='k') : ?>checked<?php endif ?>>Kobieta<br>
        <input type ="radio" name="gender" value="m"<?php  if ($gender =='m') : ?>checked<?php endif ?>>Mężczyzna<br>
        <label for = "height">Wzrost (w centymetrach)</label>
        <input type="text" name ="height" class="form-control" value="<?php echo $height ?>"><br>
        <label for="power">Tężyzna:</label><br>
        <input type="radio" name ="power" value="1"  <?php  if ($power =='1') : ?>checked<?php endif ?>> Niska<br>
        <input type="radio" name ="power" value="2"  <?php  if ($power =='2') : ?>checked<?php endif ?>>Średnia<br>
        <input type="radio" name ="power" value="3" <?php  if ($power =='3') : ?>checked<?php endif ?>>Wysoka<br>
        <input type="radio" name ="power" value="4" <?php  if ($power =='4') : ?>checked<?php endif ?>>Pro<br>
        <input type="submit" value="zapisz" class="btn btn-info">
    </form>

</main>
<footer>
    @2018 Archer Team
</footer>
</body>
</html>
