<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.12.2018
 * Time: 23:27

 */
include('scripts/session.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Profil użytkownika</title>
        <link rel="Shortcut icon" href="images/favicon.png" />
        <link rel="stylesheet" type="text/css" href="styles/style.css">
        <script type="text/javascript" src="scripts/slider.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body onload="f()">
        <header class="container">
            <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>
            <span style="font-size:45px;cursor:pointer" onclick="openNav()" id ="button">&#9776;</span>
        </header>
        <nav>
            <div id ="mySidenav" class = "sidenav">
                <a href="javascript:void(0)" class="closebtn fas fa-times" onclick="closeNav()"></a>
                <a href = "index.php" class="fas fa-home"></a>
                <a href = "profil.php">Twój profil</a>
                <a href="chooseWeapon.php">Dobór łuku</a>
                <a href="chooseArrow.php">Dobór strzał</a>
                <a href="facebook.com" class="fab fa-facebook"></a>
            </div>
        </nav>
        <main>
            <table class="table table-bordered" id="profil">
                <tr>
                    <th>Płeć</th>
                    <th>Wzrost [cm]</th>
                    <th>Poziom tężyzny</th>
                    <th>Długość naciągu [cali]</th>
                    <th>Długość łuku [cali]</th>
                    <th>Siła naciągu [kilogramy]</th>
                </tr>
                <tr>
            <?php
                include("scripts/show.php");
            ?>
                </tr>
            </table>
            <br>


                <script>
                    var gender = document.getElementById("gender").innerText;
                    var height = document.getElementById("height").innerText;
                    var power = document.getElementById("power").innerText;
                    var naciag = (height /2.54)/ 2.5;
                    var dlugosc_naciagu = naciag.toLocaleString(undefined,{minimumFractionDigits:3,maximumFractionDigits:3});
                    console.log(dlugosc_naciagu);
                    if(naciag<=24){
                        var dlugosc_luku= "60-64 cala";
                    }else if(naciag>=24 &&naciag<=26){
                        var dlugosc_luku="65-66 cala";
                    }else if(naciag >=26&&naciag<=28){
                        var dlugosc_luku="67-68 cala";
                    }else {
                        var dlugosc_luku="69-70 cala";
                    }
                    console.log(dlugosc_luku);

                    if(gender = 'k'){
                        switch (power) {
                            case '1':
                                var sila_naciagu = "mniejszy niz 10";
                                break;
                            case '2':
                                var sila_naciagu = "10kg - 12";
                                break;
                            case '3':
                                var  sila_naciagu = "12kg - 20";
                                break;
                            case '4':
                                var  sila_naciagu = "większy niż 20";
                                break;
                        }
                    }
                    else if(gender='m'){
                        switch (power) {
                            case '1':
                                var  sila_naciagu = "mniejszy niz 12";
                                break;
                            case '2':
                                var  sila_naciagu = "12kg - 14";
                                break;
                            case '3':
                                var sila_naciagu = "14kg - 20";
                                break;
                            case '4':
                                var  sila_naciagu = "większy niż 20";
                                break;
                        }
                    }
                        console.log(sila_naciagu);
                    function f() {
                        document.getElementById('dlugosc_naciagu').innerHTML = dlugosc_naciagu;
                        document.getElementById('dlugosc_luku').innerHTML = dlugosc_luku;
                        document.getElementById('sila_naciagu').innerHTML = sila_naciagu;
                    }
                    </script>

        </main>
        <footer>
            @2018 Archer Team
        </footer>
    </body>
</html>
