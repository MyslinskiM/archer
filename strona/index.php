<?php
include("scripts/session.php")
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Archer</title>
		<link rel="Shortcut icon" href="images/favicon.png" />
        <script type="text/javascript" src="scripts/slider.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="styles/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
	<body>
		<header class="container">
            <a href="index.php"><img src="images/logo.png" class="img-responsive"></a>
            <?php  if (!isset($_SESSION['username'])) : ?>
            <a href="Register.php" class="btn btn-info">Rejestruj</a>
            <a href="Login.php" class="btn btn-info">Loguj</a>
            <?php endif ?>


            <?php if (isset($_SESSION['success'])) : ?>

                <div class="error success" ng-app="myApp" ng-controller="myCtrl">

                    <h2>
                        <?php
                        echo "<div class='alert alert-success'>".$_SESSION['success']."</div>";
                        unset($_SESSION['success']);
                        ?>
                    </h2>
                </div>
            <?php endif ?>

            <?php  if (isset($_SESSION['username'])) : ?>
                <span style="font-size:45px;cursor:pointer" onclick="openNav()" id ="button">&#9776;</span>
                <p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
                <p > <a href="index.php?logout='1'" class=" btn btn-danger">logout</a> </p>

            <?php endif ?>
		</header>
		<nav>
            <div id ="mySidenav" class = "sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <a href = "index.php"><img src="images/home.png" class="icon"></a>
                <a href = "profil.php">Twój profil</a>
                <a href = "user.php">Ustawienia konta</a>
                <a href="chooseWeapon.php">Dobór łuku</a>
                <a href="chooseArrow.php">Dobór strzał</a>
				<?php  if ($_SESSION['username'] == 'doktor') : ?>
        <a href="crudEdit.php">Edycja Strony głównej</a>
        <?php endif ?>
                <a href="facebook.com"><img src = "images/fb.png" class="icon"></a>
            </div>

        </nav>
		<main>
            <div class="container"  >
                <?php
				include('scripts/crud.php')
				?>
            </div>
		</main>
		<footer>
            @2018 Archer Team
		</footer>
	</body>
</html>